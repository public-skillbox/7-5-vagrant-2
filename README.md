# 7-5-vagrant-2



## Task1
Launch an application from the repository

1. Install VirtualBox version less than 6.1.28.
2. Download the Ubuntu focal64 ISO image for VirtualBox.
3. Make sure you have Ansible installed.
4. Get the code from the repositories.
5. Change the repository variable in the Ansible playbook to point to your skillbox-deploy-blue-green repository (the original repository).
6. Set up and run the virtual machine in VirtualBox with the following parameters: Ubuntu focal64, network 10.10.10.10 - and run ansible-playbook reactjs.yaml -i "10.10.10.10".
7. Check that the site opens in your browser at http://10.10.10.10.


### Task 2. 
Launching an application from a local folder

1. Download our repository to the next directory, you will have to make adjustments.
2. Comment out the following tasks in the Ansible playbook:
     nginx installation,
     repository cloning,
     launching yarn build,
     creating a symlink,
     installing npm packages.
3. Delete the previously created virtual machine.
4. Set up and run the virtual machine in VirtualBox, forwarding the application folders to the virtual machine: ../skillbox-deploy-blue-green/":"/var/www/releases/local".
5. Connect to the created virtual machine.
6. Go to the folder where your application is now stored cd /var/www/releases/local.
7. Run yarn start.
8. After the server starts, try going to http://10.10.10.10:3000 in your browser.
9. Add a first and last name to the App.js file
10. Stop the server using the keyboard shortcut Ctrl + C.
11  . Restart the yarn start service.
12. Create your own repository and push the resulting Ansible code there.
